/** ES6 ya que se usa babel para transpilar modern js en vanilla js */

import Vue from 'vue';
import App from './App.vue';
// import Ninjas from './components/Ninjas.vue';

Vue.config.productionTip = false;

/** Declaración de componentes. Los componentes que sean de los files de Vue se pueden
 * registrar en la aplicación para su uso de dos formas.
 * Local: Dentro de un file de vue, lo que se llama nesting de componentes. Dichos componentes
 * sólo podrán ser utilizados dentro del componente padre
 * Global: Los componentes globales son registrados como Vue.component('', {}) y se
 * declararán aquí. Estos componentes podrán ser utilizados en toda la app sin restricciones
 * adicionales.
 * Las delcaraciones aparentemente son case sensitive en modo global
 */

/** Declaración global 
Vue.component('ninjas', Ninjas);
*/

/** Event Bus 
 * El bus es una instancia de vue que permite comunicación entre componentes a través de eventos
 * donde se utilizan el $emit y el $on para emitirlos y recibirlos respectivamente
 */
export const bus = new Vue({});

new Vue({
  /** Toma el root component (App) y lo renderiza al div que existe en index.html con
   * la función mount. El loader inyecta este script aunque no esté presente la declaración
   * en el html
   */
  render: h => h(App)
}).$mount('#app');