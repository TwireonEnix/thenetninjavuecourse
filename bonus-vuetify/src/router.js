import Vue from 'vue'
import Router from 'vue-router'
import FirstDemo from './views/FirstDemo'
import Breakpoints from './views/Breakpoints'
import Home from './views/Home'
import Dashboard from './views/Dashboard'
import Projects from './views/Projects'
import Team from './views/Team'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/demo',
      name: 'demo',
      component: FirstDemo
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import( /* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/br',
      name: 'breakpoints',
      component: Breakpoints
    },
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects
    },
    {
      path: '/team',
      name: 'team',
      component: Team
    }
  ]
})