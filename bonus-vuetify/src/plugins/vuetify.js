import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  /** en la configuración de Vuetify se puede hacer override del theme que controla los
   * colores de primary warn info y success, error
  theme: {
    primary: Hexcode
    error: #asdasd
    ...
  }
  */
})