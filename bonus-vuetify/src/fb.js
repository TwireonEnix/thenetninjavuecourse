import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
  /** Identifica la aplicación del frontend en el backend */
  apiKey: "AIzaSyC6yuGR_X0LBLgenyY0_3SYtbbiS5bA194",
  authDomain: "vuetify-course-tutorial.firebaseapp.com",
  databaseURL: "https://vuetify-course-tutorial.firebaseio.com",
  projectId: "vuetify-course-tutorial",
  storageBucket: "vuetify-course-tutorial.appspot.com",
  messagingSenderId: "1087175195765"
};

firebase.initializeApp(config);
const db = firebase.firestore();

db.settings({
  timestampsInSnapshots: true
})

export default db;