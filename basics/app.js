/** Instancia de Vue: su rol es controlar toda o partes de la aplicación
 * cada parte (componente es controlada por una instancia vue distinta)
 * Opciones de vue:
 * el: cuál elemento de la página va a ser controlada por la instancia de vue,
 * la instancia crea la conexión entre ella y el dom.
 *
 * Data: datos de la instancia, es posible tener n número de atributos en la instancia.
 * Metodos: Para llamar desde el dom, pueden recibir parármetros desde el template y
 * utilizar los datos en la instancia (data), al acceder a las propiedades la instacia
 * de vue hace un proxy hacia arriba para acceder a través de la keyword this.
 * entonces para acceder es posible con this.name, en lugar de this.data.name
 *
 * Directivas: Le dicen a la instancia que haga algo
 * v-bind: data binding hacia las propiedades de las etiquetas html, shorthand : (only colon)
 * v-html: convierte un string que contenga markup válido en un elemento en el dom
 * v-on: crea event listeners para disparar alguna funcionalidad, aqui dentro no es necesario
 *       referenciar a los métodos con (), puesto que el evento asume que es una función que
 *       buscará dentro de la instancia. Sin embargo es necesario utilizar () cuando se pasan
 *       parámetros o cuando se utiliza en un string interpolation.
 *       NOTA: al parecer el parámetro que se envía cuando no se utilizan paréntesis es evento
 *       en sí, por lo tanto si se quiere pasar un undefined se colocan. Shorthand @
 *
 * Event modifiers: Modifican el comportamiento de eventos ej:
 * Escuchar sólo una vez @click.once, en los keyboard events es posible agregar modificadores
 * de teclas y encadenarlas ej @keyup.alt.enter
 *
 * Two Way Binding con v-model: Cuando un dato cambie en el input se actualizará en todas las instancias
 * de la variable, tanto para hacer output como en el input.
 *
 * Computed Properties:
 * Cada vez que un valor cambia en el dom dentro de la instancia que vue controla, vue revisa cada
 * función y la volverá a ejecutar, sin importar que haya cambiado un valor que no tenga que ver
 * con el método, lo cual genera un poco de performance impact, aunque no lo necesite. Para esto se
 * utilizan las computed properties
 *
 * Conditionals:
 * v-if: que evalúa un boolean, v-if desaparece el objeto del dom en caso de ser falso
 * v-show, mismo que v-if pero éste agrega el estilo de display: none al elemento, no lo remueve del
 * dom
 * v-else: no funciona con v-show, y únicamente funciona si está declarado inmediatamente después del
 * v-if
 *
 * Loops
 * Recorrer los arrays y objetos con v-for, para los arrays: (value, index) of array
 * para recorrer objetos sin necesidad de saber los nombres de las propiedades:
 * (value, key) of Object
 * 
 * Components
 * Vue.component('nombreDelComponente', {
 *  template: 'someHTML',
 * })
 * 
 * Al registrarlos se pueden ulitizar dentro de los elementos controlados por instancias de Vue
 * como etiquetas html, <nombreDelComponente />
 * En los componentes el atributo data debe ser una función que regrese un objeto con los datos, esto
 * es porque cuando se usan los componentes tienen sus datos individuales, y registrandolos como un
 * objeto, todos harían referencia al mismo data. Entonces cambiando los datos a ser una función, cada
 * instancia del componente tiene sus datos encapsulados dentro de él.
 * 
 * Refs
 * Vue provee una manera de acceder a elementos html a través de una directiva llamada ref
 * <input type="text" ref="something"/>
 * 
 * Entonces en la isntancia se puede acceder a todas las propiedades del objeto con this.$refs
 * ($ está reservada para todos los atributos y métodos nativos de la instancia de vue).
 * en el ejemplo: para acceder al valor de la etiqueta html sería con
 * this.$refs.something.value
 * 
 * Los refs se pueden colocar en prácticamente todos los elementos html para acceder a sus propiedades
 */
{
  /** Instance basics */
  new Vue({
    el: '#vue-app',
    data: () => ({
      name: 'Darío',
      job: 'Web Developer',
      website: 'https://9gag.com',
      websiteTag: `<a href="https://google.com">Google</a>`
    }),
    methods: {
      greet(time) {
        return `Good ${time} ${this.name}`;
      }
    }
  });

  /** Events */
  new Vue({
    el: '#vue-events',
    data: () => ({
      age: 28,
      x: 0,
      y: 0
    }),
    methods: {
      add(flag) {
        flag ? (this.age += 10) : this.age++;
      },
      subtract(flag) {
        flag ? (this.age -= 10) : this.age--;
      },
      updateCoordenates($event) {
        this.x = $event.offsetX;
        this.y = $event.offsetY;
      },
      leaving() {
        alert('You clicked me');
      }
    }
  });

  /** Keyboard events and Computed properties*/
  new Vue({
    el: '#vue-kbe-computed',
    data: () => ({
      name: '',
      age: '',
      age2: 20,
      a: 0,
      b: 0
    }),
    methods: {
      log() {
        console.log('you entered your name');
      },
      logAge() {
        console.log('you entered your age');
      }
    },
    computed: {
      addToA() {
        // console.log('add to a')
        return this.a + this.age2;
      },
      addToB() {
        // console.log('add to b')
        return this.b + this.age2;
      }
    }
  });

  /** Conditionals and loops */
  new Vue({
    el: '#vue-conditionals-loops',
    data: () => ({
      error: false,
      success: false,
      characters: ['Mario', 'Luigi', 'Yoshi', 'Bowser'],
      ninjas: [{
          name: 'Ryu',
          age: 25
        },
        {
          name: 'Yoshi',
          age: 35
        },
        {
          name: 'Ken',
          age: 55
        }
      ]
    })
  });
}

/** Project Code */

new Vue({
  el: '#small-project',
  data: () => ({
    health: 100,
    end: false,
    dmg: []
  }),
  methods: {
    punch() {
      const dmg = Math.floor(Math.random() * 10 - 1 + 1) + 1;
      this.health -= dmg;
      this.dmg.push(dmg);
      if (this.health <= 0) {
        this.health = 0;
        this.end = true;
      }
    },
    restart() {
      this.dmg = [];
      this.health = 100;
      this.end = false;
    }
  },
  computed: {}
});

/** Components */

const data = {
  name: 'Yoshi'
};

Vue.component('greeting', {
  template: `<div>
    <p>Hi there, I'm a reusable component, and my name is {{ name }}</p>
    <button @click="changeName">Change name</button>
    </div>`,
  /** Cambiando el atributo data por un objeto para hacer la prueba de que
   * cambia en ambas instancias del componente
   */
  // data: () => data,
  data: () => ({
    ...data
  }),
  methods: {
    changeName() {
      this.name = 'Mario'
    }
  }
})

new Vue({
  el: '#vue-components1',
})

new Vue({
  el: '#vue-components2',
})

/** Refs */
new Vue({
  el: '#vue-refs',
  data: () => ({
    output: 'Your fav food'
  }),
  methods: {
    readRefs() {
      const {
        value
      } = this.$refs.input;
      console.log(this.$refs.paragraph.innerText);
      this.output = value;
    }
  }
})